from django.shortcuts import render
from django.http import HttpResponse
import random

# Create your views here.
def home(request):
    return render(request, 'home.html')

def generatedPassword(request):
    #* Guardamos la password generada
    generated_password = ''

    #* Se crea la lista inicial
    characters = list('abcdefghijklmnopqrstuvwxyz')

    #! request.GET.get - Nos permite capturar los valores del formulario
    #! .extends - Agrega nuevos elementos a la lista

    if request.GET.get('uppercase'):
        characters.extend(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))

    if request.GET.get('special'):
        characters.extend(list('!@#$%^&*(){}[]'))

    if request.GET.get('numbers'):
        characters.extend(list('1234567890'))

    length = int(request.GET.get('length'))

#* Genera la longitud seleccionada del password
    for x in range(length):
        generated_password += random.choice(characters)

#! {'password': generated_password} - Retorna el valor para el consumo del html
    return render(request, 'password.html', {'password': generated_password})